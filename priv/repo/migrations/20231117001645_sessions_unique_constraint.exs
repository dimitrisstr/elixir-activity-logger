defmodule ActivityLogger.Repo.Migrations.SessionsUniqueConstraint do
  use Ecto.Migration

  def change do
    drop index(:sessions, [:activity_id])
    drop index(:sessions, :session_date)
    create unique_index(:sessions, [:session_date, :activity_id])
  end
end
