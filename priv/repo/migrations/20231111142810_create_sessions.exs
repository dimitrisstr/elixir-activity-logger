defmodule ActivityLogger.Repo.Migrations.CreateSessions do
  use Ecto.Migration

  def change do
    create table(:sessions) do
      add :session_date, :date, null: false
      add :duration, :time, null: false
      add :activity_id, references(:activities, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:sessions, [:activity_id])
    create index(:sessions, :session_date)
  end
end
