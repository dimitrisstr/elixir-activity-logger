defmodule ActivityLogger.Repo.Migrations.CreateTags do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :name, :string, null: false

      timestamps()
    end

    create table(:activities_tags, primary_key: false) do
      add :activity_id, references(:activities, on_delete: :delete_all), null: false
      add :tag_id, references(:tags, on_delete: :delete_all), null: false
    end

    create unique_index(:activities_tags, [:activity_id, :tag_id])
  end
end
