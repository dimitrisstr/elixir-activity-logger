defmodule ActivityLogger.Repo.Migrations.CreateActivities do
  use Ecto.Migration

  def change do
    create table(:activities) do
      add :name, :string, null: false
      add :description, :string
      add :completed, :boolean, default: false, null: false

      timestamps()
    end
  end
end
