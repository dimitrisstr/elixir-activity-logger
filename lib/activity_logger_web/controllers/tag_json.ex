defmodule ActivityLoggerWeb.TagJSON do
  alias ActivityLogger.Tags.Tag

  @doc """
  Renders a list of tags and returns an object.
  """
  def index(%{tags: tags}) do
    %{data: nested_index(%{tags: tags})}
  end

  @doc """
  Renders a single tag.
  """
  def show(%{tag: tag}) do
    %{data: data(tag)}
  end

  @doc """
  Renders a list of tags and returns a list.
  """
  def nested_index(%{tags: tags}) do
    for(tag <- tags, do: data(tag))
  end

  defp data(%Tag{} = tag) do
    %{
      id: tag.id,
      name: tag.name
    }
  end
end
