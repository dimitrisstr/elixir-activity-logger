defmodule ActivityLoggerWeb.PageHTML do
  use ActivityLoggerWeb, :html

  embed_templates "page_html/*"
end
