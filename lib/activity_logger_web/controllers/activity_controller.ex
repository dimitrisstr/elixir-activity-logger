defmodule ActivityLoggerWeb.ActivityController do
  use ActivityLoggerWeb, :controller

  alias ActivityLogger.Activities
  alias ActivityLogger.Activities.Activity
  alias ActivityLogger.Sessions.Session

  action_fallback ActivityLoggerWeb.FallbackController

  def index(conn, _params) do
    activities = Activities.list_activities()
    render(conn, :index, activities: activities)
  end

  def create(conn, %{"activity" => activity_params}) do
    with {:ok, %Activity{} = activity} <- Activities.create_activity(activity_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/activities/#{activity}")
      |> render(:show, activity: activity)
    end
  end

  def show(conn, %{"id" => id}) do
    activity = Activities.get_activity!(id)
    render(conn, :show, activity: activity)
  end

  def update(conn, %{"id" => id, "activity" => activity_params}) do
    activity = Activities.get_activity!(id)

    with {:ok, %Activity{} = activity} <- Activities.update_activity(activity, activity_params) do
      render(conn, :show, activity: activity)
    end
  end

  def delete(conn, %{"id" => id}) do
    activity = Activities.get_activity!(id)

    with {:ok, %Activity{}} <- Activities.delete_activity(activity) do
      send_resp(conn, :no_content, "")
    end
  end

  def create_session(conn, %{"id" => id, "session" => session_params}) do
    activity = Activities.get_activity!(id)
    with {:ok, %Session{} = session} <- Activities.create_session(activity, session_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/sessions/#{session}")
      |> put_view(json: ActivityLoggerWeb.SessionJSON)
      |> render(:show, session: session)
    end
  end

  def index_sessions(conn, %{"id" => id}) do
    sessions = Activities.get_activity!(id) |> Activities.list_sessions
    conn
    |> put_view(json: ActivityLoggerWeb.SessionJSON)
    |> render(:index, sessions: sessions)
  end

  def index_tags(conn, %{"id" => id}) do
    tags = Activities.get_activity!(id) |> Activities.list_tags
    conn
    |> put_view(json: ActivityLoggerWeb.TagJSON)
    |> render(:index, tags: tags)
  end

  def update_tags(conn, %{"id" => id}) do

  end

end
