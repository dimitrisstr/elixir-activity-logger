defmodule ActivityLoggerWeb.SessionJSON do
  alias ActivityLogger.Sessions.Session

  @doc """
  Renders a list of sessions.
  """
  def index(%{sessions: sessions}) do
    %{data: for(session <- sessions, do: data(session))}
  end

  @doc """
  Renders a single session.
  """
  def show(%{session: session}) do
    %{data: data(session)}
  end

  defp data(%Session{} = session) do
    %{
      id: session.id,
      session_date: session.session_date,
      duration: session.duration,
      activity_id: session.activity_id,
      inserted_at: session.inserted_at,
      updated_at: session.updated_at
    }
  end
end
