defmodule ActivityLoggerWeb.ActivityJSON do
  alias ActivityLogger.Activities.Activity

  @doc """
  Renders a list of activities.
  """
  def index(%{activities: activities}) do
    %{data: for(activity <- activities, do: data(activity))}
  end

  @doc """
  Renders a single activity.
  """
  def show(%{activity: activity}) do
    %{data: data(activity)}
  end

  defp data(%Activity{} = activity) do
    %{
      id: activity.id,
      name: activity.name,
      description: activity.description,
      completed: activity.completed,
      tags: ActivityLoggerWeb.TagJSON.nested_index(%{tags: activity.tags}),
      inserted_at: activity.inserted_at,
      updated_at: activity.updated_at
    }
  end
end
