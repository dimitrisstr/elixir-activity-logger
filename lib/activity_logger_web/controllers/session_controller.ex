defmodule ActivityLoggerWeb.SessionController do
  use ActivityLoggerWeb, :controller

  alias ActivityLogger.Sessions
  alias ActivityLogger.Sessions.Session

  action_fallback ActivityLoggerWeb.FallbackController

  def index(conn, _params) do
    sessions = Sessions.list_sessions(conn.query_params)
    render(conn, :index, sessions: sessions)
  end

  def show(conn, %{"id" => id}) do
    session = Sessions.get_session!(id)
    render(conn, :show, session: session)
  end

  def update(conn, %{"id" => id, "session" => session_params}) do
    session = Sessions.get_session!(id)

    with {:ok, %Session{} = session} <- Sessions.update_session(session, session_params) do
      render(conn, :show, session: session)
    end
  end

  def delete(conn, %{"id" => id}) do
    session = Sessions.get_session!(id)

    with {:ok, %Session{}} <- Sessions.delete_session(session) do
      send_resp(conn, :no_content, "")
    end
  end
end
