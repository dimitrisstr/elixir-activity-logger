defmodule ActivityLoggerWeb.Layouts do
  use ActivityLoggerWeb, :html

  embed_templates "layouts/*"
end
