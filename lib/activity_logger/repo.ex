defmodule ActivityLogger.Repo do
  use Ecto.Repo,
    otp_app: :activity_logger,
    adapter: Ecto.Adapters.Postgres
end
