defmodule ActivityLogger.Sessions.Session do
  use Ecto.Schema
  import Ecto.Changeset

  schema "sessions" do
    field :duration, :time
    field :session_date, :date
    field :activity_id, :id

    timestamps()
  end

  @doc false
  def changeset(session, attrs) do
    session
    |> cast(attrs, [:session_date, :duration])
    |> validate_required([:session_date, :duration, :activity_id])
    |> unique_constraint([:session_date, :activity_id])
  end
end
