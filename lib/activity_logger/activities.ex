defmodule ActivityLogger.Activities do
  @moduledoc """
  The Activities context.
  """

  import Ecto.Query, warn: false
  alias ActivityLogger.Repo

  alias ActivityLogger.Activities.Activity
  alias ActivityLogger.Tags.Tag
  alias ActivityLogger.Sessions

  @doc """
  Returns the list of activities.

  ## Examples

      iex> list_activities()
      [%Activity{}, ...]

  """
  def list_activities do
    Repo.all(Activity) |> Repo.preload(:tags)
  end

  @doc """
  Gets a single activity.

  Raises `Ecto.NoResultsError` if the Activity does not exist.

  ## Examples

      iex> get_activity!(123)
      %Activity{}

      iex> get_activity!(456)
      ** (Ecto.NoResultsError)

  """
  def get_activity!(id) do
    Repo.get!(Activity, id) |> Repo.preload(:tags)
  end

  @doc """
  Creates an activity.

  ## Examples

      iex> create_activity(%{field: value})
      {:ok, %Activity{}}

      iex> create_activity(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_activity(attrs \\ %{}) do
    %Activity{}
    |> change_activity(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates an activity.

  ## Examples

      iex> update_activity(activity, %{field: new_value})
      {:ok, %Activity{}}

      iex> update_activity(activity, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_activity(%Activity{} = activity, attrs) do
    activity
    |> change_activity(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes an activity.

  ## Examples

      iex> delete_activity(activity)
      {:ok, %Activity{}}

      iex> delete_activity(activity)
      {:error, %Ecto.Changeset{}}

  """
  def delete_activity(%Activity{} = activity) do
    Repo.delete(activity)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking activity changes.

  ## Examples

      iex> change_activity(activity)
      %Ecto.Changeset{data: %Activity{}}

  """
  def change_activity(%Activity{} = activity, attrs \\ %{}) do
    tags = list_tags_by_id(attrs["tag_ids"])

    activity
    |> Repo.preload(:tags)
    |> Activity.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:tags, tags)
  end

  def list_tags_by_id(nil), do: []
  def list_tags_by_id(tag_ids) do
    Repo.all(from t in Tag, where: t.id in ^tag_ids)
  end

  @doc """
  Creates a session for an activity.

  ## Examples

      iex> create_session(activity, %{field: value})
      {:ok, %Session{}}

      iex> create_session(activity, %{field: bad_value})
      {:error, %Ecto.Changeset{}}
  """
  def create_session(%Activity{} = activity, attrs \\ %{}) do
    activity
    |> Ecto.build_assoc(:sessions)
    |> Sessions.change_session(attrs)
    |> Repo.insert()
  end

  @doc """
  Returns the sessions of an activity.

  ## Examples

      iex> list_sessions(activity)
      [%Session{}, ...]

  """
  def list_sessions(%Activity{} = activity) do
    activity
    |> Repo.preload(:sessions)
    |> Map.get(:sessions)
  end

  @doc """
  Returns the tags of an activity.

  ## Examples

      iex> list_tags(activity)
      [%Tag{}, ...]

  """
  def list_tags(%Activity{} = activity) do
    activity
    |> Repo.preload(:tags)
    |> Map.get(:tags)
  end
end
