defmodule ActivityLogger.Activities.Activity do
  use Ecto.Schema
  import Ecto.Changeset

  schema "activities" do
    field :name, :string
    field :description, :string
    field :completed, :boolean, default: false
    has_many :sessions, ActivityLogger.Sessions.Session
    many_to_many :tags, ActivityLogger.Tags.Tag, join_through: "activities_tags", on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(activity, attrs) do
    activity
    |> cast(attrs, [:name, :description, :completed])
    |> validate_required([:name, :completed])
  end
end
