defmodule ActivityLogger.SessionsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `ActivityLogger.Sessions` context.
  """

  @doc """
  Generate a session.
  """
  def session_fixture(attrs \\ %{}) do
    {:ok, session} =
      attrs
      |> Enum.into(%{
        duration: ~T[14:00:00],
        session_date: ~D[2023-11-10]
      })
      |> ActivityLogger.Sessions.create_session()

    session
  end
end
