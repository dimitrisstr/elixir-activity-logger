defmodule ActivityLogger.ActivitiesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `ActivityLogger.Activities` context.
  """

  @doc """
  Generate a activity.
  """
  def activity_fixture(attrs \\ %{}) do
    {:ok, activity} =
      attrs
      |> Enum.into(%{
        completed: true,
        description: "some description",
        name: "some name"
      })
      |> ActivityLogger.Activities.create_activity()

    activity
  end
end
